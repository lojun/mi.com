$(function(){
    //获取一下web本地存储空间的data及cookie中的用户名
    let data = localStorage.getItem('data');
    let username = getCookie('username');
    //获取的数据是json格式的 所以将data数据转换一下
    let dataStr = JSON.parse(data);//转换后的数据是一个数组，数组内每一项存储的都是对象，需要获取对象中的n 并把n相加 [{"username":"bbb","id":"115","n":7},{"username":"bbb","id":"116","n":1}]
    //对数组的每一项进行判断
    let n = 0;//用一个数值来获取n
    dataStr.map(v=>{
        if(v.username === username){//当数组对象中的用户名和cookie中的用户名一致
            return n += v.n-0;//将每个对象中的n累加起来
        }
    })
    $('.cartnum').text(n);
})