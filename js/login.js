$(function () {
    //判断其它页面跳转过来是否是注册页面（是否传递了‘register’的cookie）再进行样式的渲染
    if (getCookie('register')=='register') {
        $('.login').css('display', 'none').next().css('display', 'block');
        $('.layout_card').css('left', '68px');
        //样式渲染完毕后，删除之前设置的cookie
        delCookie('register');
    }
    //点击登录/注册下的横线滑动 及登录注册的切换
    $('.tablist').on('click', 'a', function () {//给tablist添加 点击事件 委托给子元素a 
        if ($(this).prop('class') == 'tablogin') {
            $('.login').css('display', 'block').next().css('display', 'none')
            $('.layout_card').css('left', '0')
        } else if ($(this).prop('class') == 'tabregister') {
            $('.login').css('display', 'none').next().css('display', 'block')
            $('.layout_card').css('left', '68px');
        }
    })

    $('.login').validate({
        rules: {
            username: 'required',
            password: 'required',
            agree:'required'
        },
        messages: {
            username: '用户名不能为空',
            password: '密码不能为空',
            agree:'请同意',
        },
        submitHandler(form) {
            var loadindex = layer.load(1, {
                shade: [0.5, '#333']
            });
            $.ajax({
                url: '../server/login.php',
                data: $(form).serialize(),
                dataType: 'json',
                method: 'post',
                success: res => {
                    var { meta: { status, msg } } = res;
                    layer.close(loadindex);
                    var msgindex = layer.msg(msg);
                    if (status === 0) {
                        setCookie('username', $('[name="username"]').val())
                        if ($('[name="remember"]').prop('checked')) {
                            setCookie('rememberusername', $('[name="username"]').val(), 7 * 24 * 60 * 60)
                        }
                        setTimeout(() => {
                            layer.close(msgindex)
                            $('.btn-login').prop('disabled', false);
                            location.href = 'index.html';
                        }, 1000)
                    } else {
                        setTimeout(() => {
                            layer.close(msgindex);
                            return false;
                        }, 1000)
                    }
                }
            })
            return false;
        }
    })

    $('.register').validate({
        rules: {//表单验证规则
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            repass: {
                required: true,
                minlength: 5,
                equalTo: '#password',
            },
            email: {
                required: true,
                email: true,
            },
            tel: {
                required: true,
                // tellength:`${/^1[3|4|5|7|8]\d{9}$/}`,//自定义规则
                tellength: true,//自定义规则
            }
        },
        messages: {//表单提示
            username: {
                required: '请输入用户名',
                minlength: '用户名长度应在两个及以上'
            },
            password: {
                required: '请输入密码',
                minlength: '密码长度不能少于5位'
            },
            repass: {
                required: '请再次输入密码',
                minlength: '密码长度不能少于5位',
                equalTo: '两次密码不一致'
            },
            email: {
                required: '请输入邮箱',
                email: '请输入正确的邮箱'
            },
            tel: {
                required: '请输入电话号码',
            }
        },
        submitHandler(form) {
            //弹出遮罩层
            var loadindex = layer.load(1, {
                shade: [0.5, '#333']
            });
            //并且禁用注册按钮
            $('.btn-register').prop('disabled',true);
            // 发起Ajax请求
            $.ajax({
                url: '../server/register.php',
                data: $(form).serialize(),
                dataType: 'json',
                method: 'post',
                success: res => {
                    layer.close(loadindex)
                    //解构赋值
                    var { meta: { status, msg } } = res;
                    var msgindex = layer.msg(msg);
                    if (status === 0) {
                        setTimeout(() => {
                            layer.close(msgindex);
                            layer.msg('请登录', {
                                icon: 6,
                                time: 3000
                            }, () => {
                                $('.login').css('display', 'block').next().css('display', 'none');
                                $('.layout_card').css('left', '0');
                            });
                        }, 1000)
                    } else {
                        // 解除禁用按钮
                        $('.btn-register').prop('disabled',false);
                        return false;
                    }
                }
            });
        }
    })
    //自定义校验手机号码规则
    $.validator.addMethod('tellength', function (val) {
        if (! /^1[3|4|5|7|8]\d{9}$/.test(val)) {
            return false;
        }
        return true;
    }, '手机号码格式不正确！')
    
})