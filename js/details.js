$(function () {
    //获取一下浏览器存储的id
    let id = sessionStorage.getItem('id');
    let num = 0;
    // console.log(id);
    if (!id || !/^\d+$/.test(id)) {
        layer.msg('数据错误!!', { time: 2000 }, function () {
            location.href = 'index.html'; // 跳转回列表页
        });
    }
    //进行加载遮蔽
    let index = layer.load(6, { shade: [0.3, '#333'] });
    (async function () {
        //ajax请求数据
        let res = await $.ajax({
            url: '../server/detail.php',
            data: { id },
            dataType: 'json',
        });
        //将数据进行解构
        let { data } = res;
        //把商品库存赋值给定义的num变量 Number()转换一下
        num = Number(data.stock);
        // console.log(data); //查看一下数据
        //把获取到的数据替换掉 页面中需要替换掉的静态资源
        $('.title-ul_left>li>h2').text(data.name);
        let arrImg = data.imgpath.split('==========');
        // console.log(arrImg);
        let str = arrImg.map(v => `<div class="swiper-slide"><img src= "${v}" alt=""></div>`
        ).join('')
        $('.swiper-wrapper').html(str);
        $('.product-con>h2').text(data.name);
        $('.sale-desc').find('span').text(data.introduce).end().find('i').text(data.detail);
        $('span.company-info').text(data.price + '元');
        $('span.edit').text(`现货(${data.stock}台)`)
        $('.selected-list>ul>li>i').eq(0).text(data.name)
        $('.selected-list>ul>li>span').text(data.price + '元');
        $('.total-price').text(`总计:${data.price}`);
        $('.btn-primary').attr('index', data.id);
        //替换结束关闭加载层
        layer.close(index);
    })()
    //这个是选择手机信息的  给 clearfix点击事件委托给他的子元素li
    $('.clearfix').on('click', 'li', function () {
        //将当前点击的li元素添加类名 active ac(他父元素的父元素的父元素的index索引下标)   再将点击的子元素添加类名active ，完毕后将点击元素的其它兄弟元素的类名及其他兄弟子元素a的类名 移除
        $(this).addClass(`active ac${$(this).parent().parent().parent().index()}`).find('a').addClass('active').end().siblings().removeClass(`active ac${$(this).parent().parent().parent().index()}`).find('a').removeClass('active');
        //选择的同时将下面显示的内容替换为选择的内容
        $('.selected-list>ul>li>i').eq(1).text($('.ac0').prop('title'));
        $('.selected-list>ul>li>i').eq(2).text($('.ac1').prop('title'));
        $('.selected-list>ul>li>i').eq(3).text($('.ac2').prop('title'));
        return false;
    })
    //给加入购物车点击事件
    $('.btn-primary').on('click', function () {
        //获取一下cookie中的用户名
        let username = getCookie('username');
        //定义n变量，加入购物车的默认值为1，在购物车进行 数量的加减
        let n = 1;
        //为空时提示用户需要登录 并跳转
        if (!username) return layer.msg('请先登录', { time: 1000 }, () => { location.href = './login.html' });
        //这里用一个对象存储 用户名，及产品id号，且 加入购物车数量默认值为1
        let car = { username, id,n};
        let carData = localStorage.getItem('data');
        if (!carData || !carData.length){//本地购物车中没有存储数据
            localStorage.setItem('data',JSON.stringify([car]));
            layer.msg('加入购物车完成',{icon:6},()=>{
                location.href = './shopcart.html';
            });
            return ; 
        }
        //当购物车中有数据时
        let carArr = JSON.parse(carData);
        //判断用户之前是否添加过产品
        let index = carArr.findIndex(v => v.username === username && v.id === id);
        //用户没有添加产品
        if(index === -1){
            carArr.push(car);
            localStorage.setItem('data',JSON.stringify(carArr));
            layer.msg('加入购物车完成',{icon:6},()=>{
                location.href = './shopcart.html';
            });
            return ;
        }
        //用户添加过产品时
        let car1 = carArr.find(v => v.username === username && v.id === id);
        //将当前数量和之前加入的数量 累加
        let carNum = (car1.n - 0) + (n - 0);
        //判断 累加的数量是否大于库存数
        if(carNum > num) return layer.msg('超出库存');
        //没有超出库存就进行赋值
        car1.n = carNum;
        localStorage.setItem('data',JSON.stringify(carArr));
        layer.msg('加入购物车完成',{icon:6},()=>{
            location.href = './shopcart.html';
        });
        return ; 
    });


})