{   
    let xms = document.querySelector('.head_nav_list').querySelectorAll('li')
    let head_nav_ContentBox = document.querySelector('.head_nav_ContentBox')
    let head_nav_content = document.querySelectorAll('.head_nav_content')
    
    // 设置进入 nav 下拉显示 和离开 nav 下拉隐藏
    for(var i = 0; i < xms.length - 2; i++) {
        xms[i].addEventListener('mouseover', function() {
            // 设置当离开 nav 后再次进入 nav 时清除 nav下拉延迟隐藏
            clearTimeout(window.timer)
            // 设置进入 nav 下拉盒子 高度
            head_nav_ContentBox.style = 'height: 230px;'
            // 设置进入 nav 时相对应的 nav下拉 显示其余隐藏
            for(var i = 0; i < head_nav_content.length; i++) {
                // 隐藏全部
                head_nav_content[i].style = 'diplay: none;'
            }
            // 显示当前对应的 nav下拉
            head_nav_content[indexValue(xms, this)].style = 'display: block;'
        })
        xms[i].addEventListener('mouseout', function() {
            // 设置离开 nav 后 nav下拉 延迟隐藏
            window.timer = setTimeout(function() {
                head_nav_ContentBox.style = 'height: 0;'
            }, 300)
        })
    }
    // 设置离开下拉 下拉隐藏 和进入下拉 下拉显示
    for(var i = 0; i < head_nav_content.length; i++) {
        head_nav_content[i].addEventListener('mouseover', function() {
            clearTimeout(window.timer)
            head_nav_ContentBox.style = 'height: 230px;'
        })
        head_nav_content[i].addEventListener('mouseout', function() {
            window.timer = setTimeout(function() {
                head_nav_ContentBox.style = 'height: 0;'
            }, 300)
        })
    }
}