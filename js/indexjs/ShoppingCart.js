{    
    let topbar_shoppingcart = document.getElementsByClassName("topbar_shoppingcart")[0]
    let shoppingcart_content = document.getElementsByClassName("shoppingcart_content")[0]

    topbar_shoppingcart.onmouseover = function() {
        clearTimeout(window.shoppingcart_timer)
        shoppingcart_content.style = 'height: 100px; color: black;'
        topbar_shoppingcart.className = 'topbar_shoppingcart topbar_shoppingcart_style'
    }

    topbar_shoppingcart.onmouseout = function() {
        shoppingcart_content.style = 'height: 0;'
        window.shoppingcart_timer = setTimeout(function() {
            topbar_shoppingcart.className = 'topbar_shoppingcart'
        }, 500)
    }

    shoppingcart_content.onmouseover = function() {
        clearTimeout(window.shoppingcart_timer)
        shoppingcart_content.style = 'height: 100px; color: black;'
        topbar_shoppingcart.className = 'topbar_shoppingcart topbar_shoppingcart_style'
    }

    shoppingcart_content.onmouseout = function() {
        shoppingcart_content.style = 'height: 0;'
        window.shoppingcart_timer
    }
}