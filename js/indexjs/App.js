{    
    let download_app = document.getElementById('download_app')
    let app_triangle = document.getElementsByClassName('app_triangle')[0]
    let app_content = document.getElementsByClassName('app_content')[0]

    download_app.onmouseover = function() {
        app_triangle.style = 'display: block;'
        app_content.style = 'height: 148px;'
    }

    download_app.onmouseout = function() {
        app_triangle.style = 'display: none;'
        app_content.style = 'height: 0;'
    }
}