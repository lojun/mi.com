{
    let rotating_buttonLeft = document.querySelector('.rotating_buttonLeft')
    let rotating_buttonRight = document.querySelector('.rotating_buttonRight')
    let rotating_background = document.querySelector('.rotating_background')
    let rotating_dot = document.querySelector('.rotating_dot').querySelectorAll('span')

    let x = 1
    // 左按钮
    rotating_buttonLeft.addEventListener('click', function() {
        x<=1? x=4: x--
        rotating_background.style.animation = "OpacityAnimation 5s;"
        rotating_background.style = 'background: url("./images/lunbodatu'+x+'.jpg"); background-size: 100%;'
        for(let i = 0; i < rotating_dot.length; i++) {
            rotating_dot[i].style = 'background: none;'
        }
        rotating_dot[x-1].style = 'background:  #BDBAB7;'
    })
    // 右按钮
    rotating_buttonRight.addEventListener('click', function() {
        x<4? x++: x=1
        rotating_background.style = 'background: url("./images/lunbodatu'+x+'.jpg"); background-size: 100%;'
        for(let i = 0; i < rotating_dot.length; i++) {
            rotating_dot[i].style = 'background: none;'
        }
        rotating_dot[x-1].style = 'background:  #BDBAB7;'
    })
    //  按钮 点击更换 按钮 背景和 轮播图背景
    for(let i = 0; i < rotating_dot.length; i++) {
        rotating_dot[i].addEventListener('click', function() {
            for(let i = 0; i < rotating_dot.length; i++) {
                rotating_dot[i].style = 'background: none;'
            }
            this.style = 'background: #BDBAB7;'
            rotating_background.style =  'background: url("./images/lunbodatu'+(indexValue(rotating_dot, this)+1)+'.jpg"); background-size: 100%;'
            x = indexValue(rotating_dot, this)+1
        })
    }
    // 自动变换背景
    let header_rotating = document.querySelector('.header_rotating')
    window.addEventListener('load', function() {
        function LBTgb() {
            x<4? x++: x=1
            rotating_background.style = 'background: url("./images/lunbodatu'+x+'.jpg"); background-size: 100%;'
            for (var i = 0; i < rotating_dot.length; i++) {
                rotating_dot[i].style = 'background: none;'
            }
            rotating_dot[x-1].style = 'background:  #BDBAB7;'
        }

        // 移入清除动画
        LBTgbtimer = setInterval(LBTgb, 5000)
        header_rotating.addEventListener('mouseover', function() {
            clearInterval(LBTgbtimer)
        })
        header_rotating.addEventListener('mouseout', function() {
            clearInterval(LBTgbtimer)
            LBTgbtimer = setInterval(LBTgb, 5000)
        })
    })
}