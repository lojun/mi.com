//查找是否登录  在cookie中寻找
$(function () {
    let username = getCookie('username');
    if(username) {
        $('.head-ul-right').html(`<li><a>${username}</a></li>
        <li><a href="javascript:;" class="loginOut">退出</a></li>
        <li>
                    <div class="topbar_shoppingcart">
                        <em></em>
                        <span class="cartnum"></span> 
                        <!-- 购物车内容 -->
                        <div class="shoppingcart_content">
                            购物车中还没有商品，赶紧选购吧！
                        </div>
                    </div>
                </li>`);
        $('.loginOut').on('click',function () {
            layer.confirm('亲，您要退出登录吗？',
                { btn: ['确定', '取消'] },
                function () {
                    //把设置的cookie删除，
                    delCookie('username');
                    delCookie('rememberusername');
                    // 
                    $('.head-ul-right').html(`<li><a href="./login.html">登录&nbsp;</a></li>
                <li><a href="./login.html">&nbsp; 注册</a></li>
                <li>
                    <div class="topbar_shoppingcart">
                        <em></em>
                        购物车 (0)
                        <!-- 购物车内容 -->
                        <div class="shoppingcart_content">
                            购物车中还没有商品，赶紧选购吧！
                        </div>
                    </div>
                </li>`);
                layer.msg('退出登录成功',{icon:1,time:1000});
                location.reload();
                },
                function(){
                    layer.msg('取消退出登录',{icon:1,time:1000});
                    return false;
                })
        })
    }
})