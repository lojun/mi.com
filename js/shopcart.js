$(function () {
    //获取cookie中用户名
    let username = getCookie('username');
    //进行判断
    if (!username) {
        layer.msg('请先登录', { time: 1000 });
        location.href = './login.html';
    } else {
        $('.bodyLogin').css('display', 'none');
        let carStr = localStorage.getItem('data');
        if (!carStr) {
            $('#top').css('display', 'block').next().css('display', 'none');
        } else {
            $('#top').css('display', 'none').next().css('display', 'block');
            let cararr = JSON.parse(carStr)
            let cartList = cararr.map(v => {
                if (v.username === username) {
                    return v.id;
                }
            })
            let cartList1 = cartList.filter(v => v !== undefined);
            if (cartList1.length === 0) {
                $('#top').css('display', 'block').next().css('display', 'none');
            } else {
                let ids = cartList1.join();
                (async function fn() {
                    let res = await $.ajax({
                        url: '../server/cart.php',
                        data: { ids },
                        dataType: 'json'
                    })
                    let { data } = res;
                    let innerStr = '';
                    data.forEach(v => {
                        let num = cararr.find(t => t.username === username && t.id === v.id).n;
                        innerStr += `<li class="item" index="${v.id}" stock="${v.stock}">
                        <div>
                          <input type="checkbox" name="checkis">
                        </div>
                        <div class="p-img">
                          <img src="${v.imgpath.split('==========')[0]}" alt="">
                        </div>
                        <div class="title">${v.name}</div>
                        <div class="p-num">
                        <button class="add">+</button>
                          <input type="number" value="${num}" max="${v.stock}" min="1" name="number" disabled>
                        <button class="reduce">-</button>
                        </div>
                        <div class="p-price">￥<span>${v.price}</span></div>
                        <div class="p-sum">￥<span>${num * v.price}</span></div>
                        <div class="p-del">
                          <button class="remove">删除</button>
                        </div>
                      </li>`
                    });
                    $('.list').append(innerStr);
                    checkedAll();
                    addReduce();
                    removeLi();
                    A(sum1);
                })()
            }
        }
    }
    //这里定义一个获取随机数函数
    function getNum(n, m) {
        return Math.floor(Math.random() * (m - n + 1) + n);
    }
    let pidArr = [2, 12, 13, 14];
    let sum1 = pidArr[getNum(0, pidArr.length - 1)]
    $('.inchange').click(async function(){
        let sum2 = pidArr[await getNum(0, pidArr.length - 1)]
        await A(sum2);
    });
    //将pid放在数组中
    async function A(num) {
        //ajax请求数据返回赋值
        let resA = await $.ajax({
            url: '../server/scenics.php',
            data: { pid: num },//pid为数组中随机的一个
            dataType: 'json'
        })
        //将获取到的ajax数据 解构赋值
        let { data: ranData } = resA;
        //定义一个空字符串
        let ranStr = '';
        //循环遍历数组将每项中的数据 添加到dom元素中 并拼接
        ranData.forEach(v => {
            ranStr += `<div class="smp">
        <img src="${v.imgpath.split('==========')[0]}" alt="">
        <span>${v.name}</span>
        <span style="color:#ff6700;">${v.price}元</span>
        <span style="color:#b0b0b0">317人好评</span>
      </div>`
        });
        //追加到页面中
        $('.pictures').html(ranStr)
    }

    //定义一个计算总数和总价的函数
    function numAll() {
        let num = 0;//总数
        let total = 0//总价
        $('input[name="checkis"]').each((i, v) => {
            // console.log($(v))
            if ($(v).prop('checked')) {
                num += $(v).parent().parent().find('input[name="number"]').val() - 0;
                total += $(v).parent().parent().find('.p-sum>span').text() - 0;
            }
        });
        if (!num) {
            $('.no-select-tip').css('display', 'block');
            $('.js-btn').css({ 'color': '#b0b0b0', 'backgroundColor': '#e0e0e0' });
        } else {
            $('.no-select-tip').css('display', 'none');
            $('.js-btn').css({ 'color': '#fff', 'backgroundColor': '#ff6700' });
        }
        $('.cart-total>em').text(num);
        $('.total-price>em').text(total);
    }
    //复选框全选按钮点击全选和取消全选函数
    function checkedAll() {
        $('#all').click(function () {
            if ($(this).prop('checked') === true) {
                $(this).parent().find('span').text('取消全选');
            } else {
                $(this).parent().find('span').text('全选');
            }
            $('input[name="checkis"]').prop('checked', $(this).prop('checked'));
            numAll();
        })
        $('input[name="checkis"]').click(function () {
            let arr = [...$('input[name="checkis"]')];
            let flg = arr.every(v => v.checked);
            $('#all').prop('checked', flg);
            numAll();
        })
    }


    //点击加减按钮函数
    function addReduce() {
        $('.add').click(function () {
            $('.reduce').prop('disabled', false);
            //获取一下input的val
            let num = $(this).next().val() - 0;
            //已经把库存给input的max属性，获取一下
            let stock = $(this).next().attr('max') - 0;
            num++;
            //判断一下num数量是否大于库存
            if (num > stock) {
                num = stock;
                layer.msg('已到达最大选购量，请您看看其它商品吧！');
                $(this).prop('disabled', true);
            }
            $(this).next().val(num);
            //获取一下商品id，将web内存中的购物车商品数量修改
            let id = $(this).parent().parent().attr('index');
            let data = JSON.parse(localStorage.getItem('data'));
            //查找当前商品数
            let car = data.find(v => v.username === username && v.id === id);
            //将新数据替换旧数据
            car.n = num;
            //覆盖掉web存储的旧数据
            localStorage.setItem('data', JSON.stringify(data));
            let total = num * ($(this).parent().next().find('span').text() - 0);
            $(this).parent().next().next().find('span').text(total);
            numAll();
        });
        $('.reduce').click(function () {
            $('.add').prop('disabled', false);
            // console.log($(this).prev().val());
            let num = $(this).prev().val() - 0;
            num--;
            if (num < 1) {
                num = 1;
                layer.msg('最低购买数为1');
                $(this).prop('disabled', true);
            }

            $(this).prev().val(num);

            let id = $(this).parent().parent().attr('index');
            let data = JSON.parse(localStorage.getItem('data'));
            //查找当前商品数
            let car = data.find(v => v.username === username && v.id === id);
            //将新数据替换旧数据
            car.n = num;
            //覆盖掉web存储的旧数据
            localStorage.setItem('data', JSON.stringify(data));
            let total = num * ($(this).parent().next().find('span').text() - 0);
            $(this).parent().next().next().find('span').text(total);
            numAll();
        });
    }
    //点击删除 
    function removeLi() {
        $('.remove').click(function () {
            let index = layer.confirm('确定要删除宝贝吗？', { icon: 6, btn: ['删除', '取消'] }, () => {
                //获取商品id
                let id = $(this).parent().parent().attr('index');
                //将之前购物车信息 获取 转换一下
                let theData = JSON.parse(localStorage.getItem('data'));
                //将不是该id的信息获取过来覆盖掉之前的
                let resData = theData.filter(v => !(v.username === username && v.id === id));
                localStorage.setItem('data', JSON.stringify(resData));
                //获取一些数据判断是否删除
                let resData2 = resData.filter(v => v.username === username);
                if (!resData2.length) {
                    location.reload();
                }
                //移除对应的li标签
                $(this).parent().parent().remove();
                numAll();
                layer.close(index);
            }, () => {
                layer.msg('已取消');
                return false
            })
        })
    }
})