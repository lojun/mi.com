$(function () {
    let index = layer.load();
    async function fn() {
        //ajax请求轮播图图片将index中的静态资源替换
        let showLideRes = await $.ajax({
            url: '../server/scenics.php',
            data: { pid: 121 },
            dataType: 'json'
        })
        //将获取的数据进行结构赋值
        let { data: lideData } = showLideRes;
        let showLide = '';
        lideData.forEach(v => {
            showLide += `<div class="swiper-slide" id='${v.id}'><img src=${v.imgpath.split('==========')[0]}></div>`
        })
        //将拼接字符串放到.swiper-wrapper中替换掉其子元素
        $('.swiper-wrapper').html(showLide);

        //ajax请求首页商品列表页面 
        //请求小米手机
        let listIndexRes = await $.ajax({
            url: '../server/scenics.php',
            data: { pid: 12 },
            dataType: 'json'
        })
        let { data: listData } = listIndexRes;
        let listIndex = '';
        listData.forEach(v => {
            listIndex += `<li id='${v.id}' class='click'>
            <img src="${v.imgpath.split('==========')[0]}" alt="">
            <div>
                <p>${v.name}</p>
                <p>${v.name}</p>
                <span>${v.price}起</span>
            </div>
        </li>`
        });
        $('.p1_item2>ul').html(listIndex);

        // 请求红米手机
        let listRedmi = await $.ajax({
            url: '../server/scenics.php',
            data: { pid: 14 },
            dataType: 'json'
        })
        let { data: redList } = listRedmi;
        let listRed = '';
        redList.forEach(v => {
            listRed += `<li id='${v.id}' class='click'>
            <img src="${v.imgpath.split('==========')[0]}" alt="">
            <div>
                <p>${v.name}</p>
                <p>${v.name}</p>
                <span>${v.price}起</span>
            </div>
        </li>`
        });
        $('.p2_item2>ul').html(listRed);

        //请求所有商品信息放到红米手机下
        let listAll = await $.ajax({
            url: '../server/list.php',
            datatype: 'json',
        })
        //获取过来的数据是json，给它转换一下
        listAll = JSON.parse(listAll);
        //再解构赋值
        let { data } = listAll;
        //这里用分页显示
        // console.log(data.length);
        layui.use(['laypage', 'layer'], function () {
            var laypage = layui.laypage;//获取分页组件
            laypage.render({
                elem: 'page',
                count: data.length,//获取所有数据
                layout: ['prv', 'page', 'next', 'limit', 'first', 'skip'],
                limits: [5, 10, 15, 20, 25, 30],
                limit: 5,
                prev: '上一页',
                next: '下一页',
                first: '首页',
                last: '末尾',
                jump: function (obj) {
                    let str = (function() {
                        var arr = [];
                        let start = obj.curr * obj.limit - obj.limit;
                        var thisData = data.concat().splice(start, obj.limit);
                        arr = thisData.map((v, i) => {
                            if(!v.introduce || !v.price || !v.stock){ return ;}else{
                            return `<li id=${v.id} class='click'>
                            <img src="${v.imgpath.split('==========')[0]}" alt="">
                            <div>
                                <p>${v.name}</p>
                                <p title='${v.introduce}'>${v.introduce}</p>
                                <span>${v.price}起</span>
                            </div>
                        </li>`}}
                        );
                        return arr.join('');
                    })();
                    $('.p3_item2>ul').html(str);
                }
            })
        })

        layer.close(index);
    }

    fn();
    
})