$(function () {
    let index = layer.load();
    (async function fn() {
        //头部隐藏的nav获取数据
        //请求小米数据
        let bjres = await $.ajax({
            url: '../server/scenics.php',
            data: { pid: 12 },
            dataType: 'json',
        })
        let { data: bjData } = bjres;
        let bjStr = ``;
        bjData.forEach((v) => {
            // console.log(v);
            bjStr += `
                <li id=${v.id} class='click'>
                    <a href="#">
                        <img src="${v.imgpath.split('==========')[0]}" alt="...">
                        <div>${v.name}</div>
                        <p>${v.price}元</p>
                    </a>
                </li>`;

        });
        $('.head_nav_content1>ul').html(bjStr);
        //请求红米数据
        let ahres = await $.ajax({
            url: '../server/scenics.php',
            data: { pid: 14 },
            dataType: 'json',
        })
        let { data: ahData } = ahres;
        let ahStr = ``;
        ahData.forEach((v) => {
            // console.log(v);
            ahStr += `
                <li id=${v.id} class='click'>
                    <a href="#">
                        <img src="${v.imgpath.split('==========')[0]}" alt="...">
                        <div>${v.name}</div>
                        <p>${v.price}元</p>
                    </a>
                </li>`
        });
        $('.head_nav_content2>ul').html(ahStr);
        layer.close(index);
    })();

    //给右上角购物车添加点击事件 跳转到购物车
    $('.topbar_shoppingcart').on('click',function(){
        //获取一下cookie中的用户名
        let username = getCookie('username');
        //进行非空判断 
        if(!username){
            layer,msg('请先登录',{ time: 1000 }, () => { location.href = './login.html' });
        }else{
            location.href = './shopcart.html'
        }
    })
    
    //购物车移入显示其子元素样式
    $('.topbar_shoppingcart').hover(
        function () {
            $(this).stop().addClass('topbar_shoppingcart_style');
            $('.shoppingcart_content').css({ height: '100px', color: 'black' })
        }, function () {
            $(this).stop().removeClass('topbar_shoppingcart_style');
            $('.shoppingcart_content').css({ height: '0px' })
        }
    )
    //因为注册登录 共用同一个页面，所以设置一个cookie过去 ，给它转一个样式，再将cookie删除
    //点击注册
    $('.index-register').click(function () {
        setCookie('register', 'register');
    })
    // 使用事件委托，给商品的li添加点击事件 
    $('ul').on('click', '.click', function () {
        //将点击的li的id值存储在sessionStorage中 
        let id = $(this).prop('id')
        sessionStorage.setItem('id', id);
        //跳转页面
        location.href = 'details.html'
        // console.log($(this).prop('id'));
    })
})